import React from 'react';
import { ConvertUnixDate, ConvertUnixTime, EmptyObject, GetIcon } from '../helpers';

describe('ConvertUnixDate', () => {
  it('Should return the start of unix time', () => {
    const expectedResult = "01 January 1970";
    const actualResult = ConvertUnixDate(0);

  	expect(actualResult).toEqual(expectedResult);
  }),
  it('Should return 28 July 2018', () => {
    const expectedResult = "28 July 2018";
    const actualResult = ConvertUnixDate(1532793277);

    expect(actualResult).toEqual(expectedResult);
  })
});

describe('ConvertUnixTime', () => {
  it('Should return 17.54', () => {
    const expectedResult = "17:54";
    const actualResult = ConvertUnixTime(1532793277);

    expect(actualResult).toEqual(expectedResult);
  })
});

describe('Empty object test', () => {
  it('Should return true if object is empty"', () => {
    const expectedResult = true;
    const obj = {};
    const actualResult = EmptyObject(obj);

    expect(actualResult).toEqual(expectedResult);
  }),
  it('Should return false if object is not empty"', () => {
    const expectedResult = false;
    const obj = {type: 'type'};
    const actualResult = EmptyObject(obj);

    expect(actualResult).toEqual(expectedResult);
  })
});

describe('Icon test', () => {
  it('Should div vit class name "class__icon--default"', () => {
    const expectedResult = <div className="class__icon--default" />;
    const actualResult = GetIcon({}, 'class');

    expect(actualResult).toEqual(expectedResult);
  })
});