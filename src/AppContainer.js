import App from './App.js';
import { connect } from 'react-redux';
import * as UserFetcher from './action-reducers/user-fetcher';
import * as ActivityFetcher from './action-reducers/activity-fetcher';

const mapDispatchToProps = dispatch => {
  return {
    fetchUser:() => dispatch(UserFetcher.fetchUser()),
    fetchUserActivity:() => dispatch(ActivityFetcher.fetchUserActivity()), 
  };
}

export default connect(null, mapDispatchToProps)(App);