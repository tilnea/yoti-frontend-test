import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk";

import * as PopupTrigger from '../action-reducers/popup-trigger';
import * as UserFetcher from '../action-reducers/user-fetcher';
import * as ActivityFetcher from '../action-reducers/activity-fetcher';

const rootReducer = combineReducers({ 
	popup: PopupTrigger.reducer, 
	user: UserFetcher.reducer,
	activities: ActivityFetcher.reducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;