import React, { Component } from 'react';
import PropTypes from 'prop-types';
import store from '../../store';
import { ConvertUnixDate, ConvertUnixTime, GetIcon } from '../../helpers';
import * as PopupTrigger from '../../action-reducers/popup-trigger';

import Selfie from '../Selfie/Selfie';


class Popup extends Component {
  closePopup = () => {
    store.dispatch(PopupTrigger.remove());
  }

  getSelfie = (array) => {
    const selfieList = array.filter((item) => {
      return Object.keys(item)[0] === 'selfie';
    });

    return selfieList[0] ? selfieList[0].selfie : '';
  }

  render() {
    const { type, application, transaction } = this.props.data;

    const isApp = type === 'application';
    const appearance = application ? application.appearance : '';
    const icon = isApp ? GetIcon(appearance, 'popup__popup__header') : '';
    const attributes = transaction.attributes;
    const otherSelfie = (type === 'share' && attributes) ? this.getSelfie(attributes) : ''; 
    const name = isApp ? application.name : 'Yoti Shared';
    const unixTimestamp = transaction["unix-timestamp"];

    const renderAttributes = attributes.map((item, i) => {
      const key = Object.keys(item)[0];

      let title = '';

      switch ( key ) {
        case 'given-names':
          title = 'Given Name(s)';
          break;
        case 'mobile-number':
          title = 'Mobile Number';
          break;
        case 'gender':
          title = 'Gender'; 
          break;
        case 'age':
           title = 'Age'; 
           break;
        case 'address':
           title = 'Adsress'; 
           break;
        case 'email-address':
           title = 'Email Address';
           break;
        default: 
          title = '';
      }
      return (title !== '') 
        ? (<div key={`attribute-${i}`}>
          <p className="popup__popup__content__attributes__title">{title}</p>
          <p className="popup__popup__content__attributes__data">{item[key]}</p>
        </div>)
        : '';
    });

    return (
      <div className="popup">
        <div className="popup__overlay" onClick={this.closePopup}></div>
        <div className="popup__popup">
          {isApp && 
            <div className="popup__popup__header" style={{backgroundColor: appearance["bg-color"]}}>
            { icon }
            </div>
          }
          <div className="popup__popup__content">
            <div className="popup__popup__content__selfie">
              <Selfie otherSelfie={otherSelfie} />
              <div className="popup__popup__content__selfie__icon"></div>
            </div>
            <div className="popup__popup__content__checkmark"></div>
            <div className="popup__popup__content__name">
              {name}
            </div>
            { isApp &&
              (<div className="popup__popup__content__info">viewd this information about you</div>)}
            <div className="popup__popup__content__timestamp">
              at {ConvertUnixTime(unixTimestamp)} on {ConvertUnixDate(unixTimestamp)}
            </div>
            <div className="popup__popup__content__attributes">
              { renderAttributes }
            </div>
          </div>
          <button className="popup__popup__close" onClick={this.closePopup}></button>
        </div>
      </div>
    );
  }
}

Popup.propTypes = {
  data: PropTypes.object.isRequired,
};


export default Popup;