import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import store from '../../store';
import { ConvertUnixDate, ConvertUnixTime, GetIcon } from '../../helpers';
import * as PopupTrigger from '../../action-reducers/popup-trigger';

class ActivityCard extends Component {

  onClick = () => {
    store.dispatch(PopupTrigger.set(this.props.data));
  }

  getDate = (unixTimestamp) => {
    const date = ConvertUnixDate(unixTimestamp);
    const now = moment().format("DD MMMM YYYY");

    if ( date === now) {
      return 'Today';
    }

    return date;
  }

  render() {
    const { data } = this.props;

    const date = this.getDate(data.transaction["unix-timestamp"]);
    const time = ConvertUnixTime(data.transaction["unix-timestamp"]);
    const type = data.type
    const name = type === "share" ? 'Yoti Shared' : data.application.name;
    const appearance = data.application ? data.application.appearance : {};
    const icon = GetIcon(appearance, 'activity-card__activity');

    return (
      <button className="activity-card" onClick={this.onClick}>
        <div className="activity-card__activity">
          {icon}
          <p className="activity-card__activity__name">{name}</p>
        </div>
        <div className="activity-card__time">
          <p>{date}</p>
          <p>{time}</p>
        </div>
      </button>
    );
  }
}

ActivityCard.propTypes = {
  data: PropTypes.object.isRequired,
};


export default ActivityCard;