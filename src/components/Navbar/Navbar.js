import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Navbar extends Component {
  render() {
    const { items } = this.props;

    const renderItems = items.map((item, i) => {
      return <li className="navbar__items__item" key={`navitem-${i}`}>
        <div className={`navbar__items__item__icon--${item.icon}`}></div>
        <p className="navbar__items__item__name">{item.name}</p>
      </li>
    });

    return (
      <nav className="navbar">
        <ul className="navbar__items">
          {renderItems}
        </ul>
      </nav>
    );
  }
}

Navbar.propTypes = {
  items: PropTypes.array.isRequired,
};

export default Navbar;