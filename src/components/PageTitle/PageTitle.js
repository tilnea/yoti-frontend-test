import React, { Component } from 'react';
import PropTypes from 'prop-types';

class PageTitle extends Component {
  render() {
  	const { title, description } = this.props;
    return (
      <div className="page-title">
      	<h5>{title}</h5>
      	<p>{description}</p>
      </div>
    );
  }
}

PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
};


export default PageTitle;