import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const mapStateToProps = state => {
  return {
    selfie: state.user.user.selfie,
  };
};

class Selfie extends Component {
  render() {
    const { selfie, otherSelfie } = this.props;
    const currentSelfie = otherSelfie ? otherSelfie : selfie;

    const hasSelfie = currentSelfie !== undefined;

    let styles = {}

    if ( hasSelfie ) {
      styles = {
        backgroundImage: `url(${currentSelfie}`
      }
    }

    return (
      <div className="selfie">
        <div className={`selfie__img${!hasSelfie ? '--default' : ''}`} style={styles}></div>
      </div>
    );
  }
}

Selfie.propTypes = {
  selfie: PropTypes.string,
  otherSelfie: PropTypes.string
};

export default connect(mapStateToProps)(Selfie);