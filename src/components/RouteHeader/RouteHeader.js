import React, { Component } from 'react';
import PropTypes from 'prop-types';

class RouteHeader extends Component {
  render() {
  	const { title } = this.props;

    return (
      <div className="route-header">
      	<h6>{title}</h6>
      </div>
    );
  }
}

RouteHeader.propTypes = {
  title: PropTypes.string.isRequired,
};


export default RouteHeader;