import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Selfie from '../Selfie/Selfie';

class Header extends Component {
  render() {
    const { connected, userData } = this.props;

    const selfie = userData && userData.selfie 
      ? userData.selfie
      : 'default';

    return (
      <header className="header">
        <div className="header__logo"></div>
        <div className="header__info">
          <div className="header__info__connect">
            <p>{connected ? 'Connected' : 'Not connected'}</p>
            <div className={ connected ? 'icon-connect-on' : 'icon-connect-off'}></div>
          </div>
          <div className="header__info__selfie">
            <Selfie src={selfie} />
          </div>
        </div>
      </header>
    );
  }
}

Header.propTypes = {
  connected: PropTypes.bool.isRequired
};

export default Header;