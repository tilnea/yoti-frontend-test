const initialState = { data: {} };

export const SET = 'SET';
export const REMOVE = 'REMOVE';

export function reducer(state = initialState, action) {

  switch ( action.type ) {
    case SET: 
      return { ...state, data: action.payload };
    case REMOVE: 
      return { ...state, data: {} };
    default:
      return state;
  }
}

export function set(payload) {
  return {
    type: SET,
    payload
  }
}

export function remove() {
  return {
    type: REMOVE
  }
}