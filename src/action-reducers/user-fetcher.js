//import axios from 'axios';
import userJSON from '../data/user.json';
const initialState = { user: {} };

export const GET_USER = 'GET_USER';

export function reducer(state = initialState, action) {
  switch ( action.type ) {
    case GET_USER: 
      return { ...state, user: action.payload }
    default:
      return state;
  }
}

export function getUser(payload) {
  return {
    type: GET_USER,
    payload
  }
}

export function fetchUser() { 
  return(dispatch) => {
    dispatch(getUser(JSON.parse(JSON.stringify(userJSON))));
  }
  //Here I would do a request to get the data about the user. I would use axios since I tried that before
  /*return(dispatch) => {
    return axios.get("api").then((res) => {
      dispatch(getUser(res.data));
    });
  }*/
}