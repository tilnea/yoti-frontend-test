//import axios from 'axios';
import activitiesJson from '../data/mock-data.json';
const initialState = { activities: [] };

export const GET_USER_ACTIVITY = 'GET_USER_ACTIVITY';

export function reducer(state = initialState, action) {
  switch ( action.type ) {
    case GET_USER_ACTIVITY: 
      return { ...state, activities: action.payload.receipts }
    default:
      return state;
  }
}

export function getUserActivity(payload) {
  return {
    type: GET_USER_ACTIVITY,
    payload
  }
}

export function fetchUserActivity() { 
  return(dispatch) => {
    dispatch(getUserActivity(JSON.parse(JSON.stringify(activitiesJson))));
  }
  //Here I would do a request to get the data about the user activity. I would use axios since I tried that before
  /*return(dispatch) => {
    return axios.get("api").then((res) => {
      dispatch(getUser(res.data));
    });
  }*/
}