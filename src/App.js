import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import './App.css';
import { EmptyObject } from './helpers';

import Activity from './routes/Activity/Activity';
import Header from './components/Header/Header';
import Navbar from './components/Navbar/Navbar';
import Popup from './components/Popup/Popup';


const mapStateToProps = state => {
  return {
    popupData: state.popup.data,
  };
};

class App extends Component {
  constructor(props) {
    super(props);

    this.navbar = {
      items: [
        {
          name: 'Activity',
          icon: 'activity'
        }
      ]
    }
  } 

  componentDidMount() {
    this.props.fetchUser();
    this.props.fetchUserActivity();
  }

  render() {
    const { popupData } = this.props;

    const popupVisable = !EmptyObject(popupData); 

    return (
      <div className="app">
        <Header connected={true} />
        <div className="app__wrapper">
          <Navbar items={this.navbar.items} />
          <div className="app__content">
            <Activity />
          </div>
        </div>
        {popupVisable && <Popup data={popupData} />}
      </div>
    );
  }
}

App.propTypes = {
  popupData: PropTypes.object,
};

export default connect(mapStateToProps)(App);