import moment from 'moment';
import React from 'react';

export function ConvertUnixDate(unixTimestamp) {
  return moment.unix(unixTimestamp).format("DD MMMM YYYY");
}

export function ConvertUnixTime(unixTimestamp) {
  return moment.unix(unixTimestamp).format("HH:mm");
}

export function EmptyObject(object) {
  return Object.getOwnPropertyNames(object).length === 0;
}

export function GetIcon(appearance, preClass) {
  if ( appearance ) {
    if ( appearance["bg-logo"] ) {
      return <div className={`${preClass}__icon`} style={{backgroundImage: `url(${appearance["bg-logo"]}`}}></div>
    } else if ( appearance["bg-img"] ) {
      return <div className={`${preClass}__icon`} style={{backgroundImage: `url(${appearance["bg-img"]}`}}></div>
    }
  }
  return <div className={`${preClass}__icon--default`}></div>
}