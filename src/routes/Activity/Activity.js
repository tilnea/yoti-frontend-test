import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import RouteHeader from '../../components/RouteHeader/RouteHeader';
import PageTitle from '../../components/PageTitle/PageTitle';
import ActivityCard from '../../components/ActivityCard/ActivityCard';
import { ConvertUnixDate } from '../../helpers';

const mapStateToProps = state => {
  return {
    activities: state.activities.activities,
  };
};

class Activity extends Component {
  render() {
  	const { activities } = this.props;
    let lastDate = '';

  	const renderActivities = activities.map((activity, i) => {
      const date =  ConvertUnixDate(activity.transaction["unix-timestamp"]);
      const activityCard = (<ActivityCard key={`activity-${i}`} data={activity} />);

      if ( lastDate !== date ) {
        lastDate = date;
        return (
          <div className="activity__first-of-date" key={`activity-header-${date}`}>
            <div className="activity__header">{date}</div>
            {activityCard}
          </div>
        );
      } 
  		return activityCard;
  	});

    return (
      <div className="activity">
      	<RouteHeader title="Activity" />
      	<PageTitle title="Activity" description="See a recod for everyone you have shared details with." />
      	{ renderActivities }
      </div>
    );
  }
}

Activity.propTypes = {
  activities: PropTypes.array,
};

export default connect(mapStateToProps)(Activity);