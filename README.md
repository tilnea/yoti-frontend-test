## Install the dependencies:
`yarn`

## Start development:
`yarn start`

## Run tests:
`yarn test`


## Summary
I really enjoyed doing this test! This is what I wanna do. It was hard for me to stop but I got the information that I should not spend to much time on it so I had to.

For the styling I saw that you included a lot of classes and variables from bootstrap. I decided not to use it all completing this test since I really like flexbox (and css actually).
I included the variables, the typography and media queries since I thing this made sense for my assignment. (I also added a color variable I found missing)

I prefer that if I want to change my styling I should do it in the css not min the html. So my approach was to extend the styling i needed to use inside my components.

I like to keep my components clean so I like to keep the css close to the component so you wont get a idea to do some other styling inside of it. I like to use the BEM naming convention to make sure the component won't get some other style on it.

I would love to have done a route as well. But since this was not a part of the assignment I did not spend any time on it.

I also want to include more tests, since I think there are some cases where thing could go wrong (especially in the popup component) but I focused on to get the page up and showed some tests for the helper functions.

I also thought about if I should parse the json and make it more in the way the activity page and popup needs it. But I also decided not to spend time on this.

I decided to use create-react-app to get up and running fast. It has everything I need and I would say it's time consuming to set it up and find all the things I want myself. I have been adding for example sass-loader to webpack projects before. But since its very easy to add sass to create react app I decided to use it.

My interpretation of "3: selfie" was that if it’s of type application, use your selfie, if its share and not have its own selfie, show your own.
In "6: Attributes section" I decided not to show the selfie attribute since I kind of got it this was not something you wanna display here. 
I hope this interpretations was ok.

I would of course love to fix more things as well. Make sure it works better to tab through as a example. And also test it with a screen reader and make sure it make sense. I did not have time to do that.


